Nodejs logger library for microservices

## INSTALL
```
npm install git@gitlab.com:nito-public/nlog.git
```

or

```
npm install https://gitlab.com/nito-public/nlog.git
```

## USAGE

```js
    const logger = require('nlog')(__filename);

    logger.debug('Debug log');
    logger.info('Info log');
    logger.warn('Warn log');
    logger.error('Error log');
```

## LEVEL
Default level is info. To change log level
```
export LOGGER_LEVEL=debug
export LOGGER_JSON=true
export LOGGER_ERROR_FILE='./log/error.log'
export LOGGER_FILE='./log/log.log'
```

or before using logger

```js
const settings = require('nlog/settings')

settings.logJson = true;
settings.logLevel = 'error';
settings.logFile = './log/log.log';
settings.errorLogFile = './log/error.log';

const logger = require('nlog')(__filename);
logger.debug('Debug log');
```

