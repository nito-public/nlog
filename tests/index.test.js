const fs = require('fs');
const readline = require('readline');

function deleteFile(filePath, silent = true) {
    try {
        fs.unlinkSync(filePath);
    } catch (err) {
        if (!silent) {
            // eslint-disable-next-line no-console
            console.error(err);
        }
    }
}

afterEach(() => {
    deleteFile('./error.log');
    deleteFile('./combined.log');
    deleteFile('./tmErr.log');
    deleteFile('./tmp.log');
});

describe('file test', () => {
    it('it should write to file', (done) => {
        const logger = require('../index')(__filename);

        expect(logger.transports.length).toBe(3);
        logger.info('Info log');

        setTimeout(() => {
            try {
                expect(fs.existsSync('./error.log')).toBeTruthy();
                expect(fs.existsSync('./combined.log')).toBeTruthy();
                done();
            } catch (error) {
                done(error);
            }
        }, 200);
    });

    it('it should write to different files', (done) => {
        const settings = require('../settings');
        settings.logFile = './tmp.log';
        settings.errorLogFile = './tmErr.log';
        const logger = require('../index')(__filename);

        expect(logger.transports.length).toBe(3);
        logger.info('Info log');

        setTimeout(() => {
            try {
                expect(fs.existsSync('./tmErr.log')).toBeTruthy();
                expect(fs.existsSync('./tmp.log')).toBeTruthy();
                done();
            } catch (error) {
                done(error);
            }
        }, 200);
    });

    it('it should not create error log file', (done) => {
        const settings = require('../settings');
        settings.logFile = './tmp.log';
        settings.errorLogFile = '';
        const logger = require('../index')(__filename);

        expect(logger.transports.length).toBe(2);
        logger.info('Info log');

        setTimeout(() => {
            try {
                expect(fs.existsSync('./tmp.log')).toBeTruthy();
                expect(fs.existsSync('./tmErr.log')).toBeFalsy();
                expect(fs.existsSync('./error.log')).toBeFalsy();
                done();
            } catch (error) {
                done(error);
            }
        }, 200);
    });

    it('it should not create error log file', (done) => {
        const settings = require('../settings');
        settings.logFile = './tmp.log';
        settings.errorLogFile = '';
        const logger = require('../index')(__filename);

        expect(logger.transports.length).toBe(2);
        logger.info('Info log');

        setTimeout(() => {
            try {
                expect(fs.existsSync('./tmp.log')).toBeTruthy();
                expect(fs.existsSync('./tmErr.log')).toBeFalsy();
                expect(fs.existsSync('./error.log')).toBeFalsy();
                done();
            } catch (error) {
                done(error);
            }
        }, 200);
    });
});

describe('caller label', () => {
    const testCases = [
        {
            name: 'default',
            caller: __filename,
            expectCaller: 'tests/index.test.js',
        },
        {
            name: 'empty',
            caller: '',
            expectCaller: 'global',
        },
        {
            name: 'custom_caller',
            caller: '/home/users/project/logger/src/test',
            expectCaller: 'src/test',
        },
        {
            name: 'custom_three_slashes',
            caller: '/logger/src/test',
            expectCaller: 'src/test',
        },
        {
            name: 'custom_two_slashes',
            caller: '/src/test',
            expectCaller: 'src/test',
        },
        {
            name: 'custom_single',
            caller: 'aaa',
            expectCaller: 'aaa',
        },
    ];

    testCases.forEach((tc) => {
        it(tc.name, (done) => {
            try {
                const logger = require('../index')(tc.caller);
                logger.info(tc.name);

                setTimeout(() => {
                    try {
                        const data = fs.readFileSync('./tmp.log', 'utf8').trim();
                        const logData = JSON.parse(data);
                        expect(logData.label).toBe(tc.expectCaller);
                        done();
                    } catch (error) {
                        done(error);
                    }
                }, 200);
            } catch (err) {
                done(err);
            }
        });
    });
});

async function normalizeLogMessages(filename = './tmp.log', isJson = false) {
    // https://nodejs.org/api/readline.html#readline_example_read_file_stream_line_by_line
    const fileStream = fs.createReadStream(filename);

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity,
    });

    const rows = [];

    // eslint-disable-next-line no-restricted-syntax
    for await (const line of rl) {
        let row = line;

        if (isJson) {
            const data = JSON.parse(line);
            data.timestamp = '0';
            if (data.stack) {
                data.stack = 'exists';
            }
            row = data;
        } else {
            // '[0] debug tests/index.test.js Debug log {JSON}'
            // Replace timestamp
            row = row.replace(/\[.*\]/, '[0]');

            // Replace json
            row = row.replace(/\{.*\}/, '{JSON}');

            // Replace tabs
            row = row.replace(/\t/g, '');
        }

        rows.push(row);
    }

    return rows;
}

describe('settings', () => {
    it('it should write default info level with json value', (done) => {
        const logger = require('../index')(__filename);
        logger.info('This is a info log');
        setTimeout(() => {
            try {
                const data = fs.readFileSync('./tmp.log', 'utf8').trim();
                const logData = JSON.parse(data);
                expect(logData.message).toBe('This is a info log');
                done();
            } catch (error) {
                done(error);
            }
        }, 200);
    });

    const allJSONLogs = [
        {
            label: 'tests/index.test.js', level: 'debug', message: 'Debug log', timestamp: '0',
        },
        {
            label: 'tests/index.test.js', level: 'info', message: 'Info log', timestamp: '0',
        },
        {
            label: 'tests/index.test.js', level: 'warn', message: 'Warn log', timestamp: '0',
        },
        {
            label: 'tests/index.test.js', level: 'error', message: 'Error log', timestamp: '0',
        },
        {
            label: 'tests/index.test.js', level: 'error', message: 'myError', timestamp: '0', stack: 'exists',
        },
    ];

    const testCases = [
        {
            name: 'log_settings with json = true, level=debug',
            json: true,
            level: 'debug',
            expectedLogs: allJSONLogs,
        },
        {
            name: 'log_settings with json = true, level=info',
            json: true,
            level: 'info',
            expectedLogs: allJSONLogs.slice(1),
        },
        {
            name: 'log_settings with json = true, level=info, level = null',
            json: true,
            level: null,
            expectedLogs: allJSONLogs.slice(1),
        },
        {
            name: 'log_settings with json = true, level=warn',
            json: true,
            level: 'warn',
            expectedLogs: allJSONLogs.slice(2),
        },
        {
            name: 'log_settings with json = true, level=error',
            json: true,
            level: 'error',
            expectedLogs: allJSONLogs.slice(3),
        },
        {
            name: 'log_settings custom level with json = true, level=info',
            json: true,
            level: 'custom',
            expectedLogs: [],
        },
        {
            name: 'log_settings with json = false, level=debug',
            json: false,
            level: 'debug',
            expectedLogs: [
                '[0] debug tests/index.test.js Debug log {JSON}',
                '[0] info tests/index.test.js Info log {JSON}',
                '[0] warn tests/index.test.js Warn log {JSON}',
                '[0] error tests/index.test.js Error log {JSON}',
                '[0] error tests/index.test.js myError {JSON}',
            ],
        },
    ];

    testCases.forEach((tc) => {
        it(tc.name, (done) => {
            const settings = require('../settings');
            settings.logJson = tc.json;
            settings.logLevel = tc.level;

            let err = null;
            const logger = require('../index')(__filename);
            try {
                throw new Error('myError');
            } catch (e) {
                err = e;
            }

            logger.debug('Debug log');
            logger.info('Info log');
            logger.warn('Warn log');
            logger.error('Error log');
            logger.error(err);

            setTimeout(async () => {
                try {
                    const rows = await normalizeLogMessages('./tmp.log', tc.json);
                    expect(rows.length).toBe(tc.expectedLogs.length);

                    for (let i = 0; i < tc.expectedLogs.length; i += 1) {
                        expect(rows[i]).toStrictEqual(tc.expectedLogs[i]);
                    }

                    done();
                } catch (error) {
                    done(error);
                }
            }, 200);
        });
    });
});
