const logLevel = process.env.LOGGER_LEVEL || 'info';
const logJson = process.env.LOGGER_JSON || true;

const errorLogFile = process.env.LOGGER_ERROR_FILE || './error.log';
const logFile = process.env.LOGGER_FILE || './combined.log';

module.exports = {
    logLevel, logJson, errorLogFile, logFile,
};
