const path = require('path');
const {
    createLogger,
    format,
    transports,
} = require('winston');

const {
    combine,
    timestamp,
    splat,
    printf,
    label,
    errors,
    json,
} = format;

const humanReadableFormat = printf((info) => `[${info.timestamp}] ${info.level}\t ${info.label}\t ${info.message} ${JSON.stringify(info)}`);

// Return the last folder name in the path and the calling
// module's filename.
const getLabel = (callingModule) => {
    if (!callingModule || callingModule.trim() === '') {
        return 'global';
    }
    const parts = callingModule.split(path.sep);
    if (parts.length < 3) {
        return callingModule.trim();
    }
    return path.join(parts[parts.length - 2], parts.pop());
};

const logger = (caller) => {
    const {
        logLevel,
        logJson,
        logFile,
        errorLogFile,
    } = require('./settings');

    const nlog = createLogger({
        level: logLevel || 'info',
        format: combine(
            errors({ stack: true }),
            label({ label: getLabel(caller) }),
            timestamp({ format: 'DD/MMM/YYYY:HH:mm:ss ZZ' }),
            splat(),
            logJson ? json() : humanReadableFormat,
        ),
        transports: [
            new transports.File({ name: 'file', filename: logFile }),
            new transports.Console({ name: 'console' }),
        ],
    });

    if (errorLogFile && errorLogFile.trim() !== '') {
        nlog.add(new transports.File({ name: 'error_file', filename: errorLogFile, level: 'error' }));
    }

    return nlog;
};

module.exports = logger;
