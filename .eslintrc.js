module.exports = {
    extends: 'airbnb-base',
    env: {
        node: true,
        jest: true,
    },
    rules: {
        'global-require': 'off',
        indent: ['error', 4],
    },
};
